import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Login from './vistas/login';
import Registro from './vistas/registro';
import Inicio from './vistas/inicio';
import Personajes from './vistas/personajes';
import Locacion from './vistas/locacion';
import Episodios from './vistas/episodios';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <Inicio />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/registro">
          <Registro />
        </Route>
        <Route path='/personajes' render={(props) => <Personajes {...props} />} />
        <Route path='/locacion' render={(props) => <Locacion {...props} />} />
        <Route path='/episodios' render={(props) => <Episodios {...props} />} />
      </Switch>
    </Router>
  );
}

export default App;