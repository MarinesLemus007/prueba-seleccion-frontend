import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

const Footer = () => {

    return (
        <footer>
            <Container>
                <Row>
                    <Col>Diseñado por Marinés Lemus</Col>
                </Row>
            </Container>
        </footer>
    );

}

export default Footer;