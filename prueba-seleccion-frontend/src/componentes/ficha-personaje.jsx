import React from 'react';
import { Card } from 'react-bootstrap';

const FichaPersonaje = (prop) => {

    return (
        <>
            <Card.Img key={prop.index} variant="top" src={prop.image} />
            <Card.Body>
                <Card.Title>{prop.name}</Card.Title>
            </Card.Body>
        </>
    );

}

export default FichaPersonaje;