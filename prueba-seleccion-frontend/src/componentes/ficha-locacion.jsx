import React from 'react';
import { Card } from 'react-bootstrap';

const FichaLocacion = (prop) => {

    return (
        < >
            <div key={prop.index}>
                <div className="dimension">
                    {prop.dimension}
                </div>
            </div>
            <Card.Body>
                <Card.Title>{prop.name}</Card.Title>
            </Card.Body>
        </>
    );

}

export default FichaLocacion;