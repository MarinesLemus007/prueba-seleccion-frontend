import React from 'react';
import { Card } from 'react-bootstrap';

const FichaEpisodios = (prop) => {

    return (
        < >
            <div key={prop.index}>
                <div className="episodios">
                    {prop.episode}
                </div>
            </div>
            <Card.Body>
                <Card.Title>{prop.name}</Card.Title>
            </Card.Body>
        </>
    );

}

export default FichaEpisodios;