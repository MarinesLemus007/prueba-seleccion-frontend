import React, { Component, Fragment } from 'react';
import { Container, Row, Col, Jumbotron, Navbar, FormControl, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import FichaPersonaje from '../componentes/ficha-personaje';
import Footer from '../componentes/footer';
import FichaLocacion from '../componentes/ficha-locacion';
import FichaEpisodios from '../componentes/ficha-episodios';

class Inicio extends Component {
    constructor() {
        super();
        this.state = {
            personajes: [],
            locacion: [],
            episodios: [],
            valorBuscador: ""
        }
        this.buscarNombre = this.buscarNombre.bind(this);
    }

    componentDidMount() {
        fetch('https://rickandmortyapi.com/api/character')
            .then(response => {
                return response.json();
            })
            .then(response => {
                this.setState(
                    {
                        personajes: response.results
                    }
                )
            })

        fetch('https://rickandmortyapi.com/api/location')
            .then(response => {
                return response.json();
            })
            .then(response => {
                this.setState(
                    {
                        locacion: response.results
                    }
                )
            })

        fetch('https://rickandmortyapi.com/api/episode')
            .then(response => {
                return response.json();
            })
            .then(response => {
                this.setState(
                    {
                        episodios: response.results
                    }
                )
            })
    }

    buscarNombre(e) {
        this.setState({
            valorBuscador: e.target.value
        });
    }

    render() {

        const { personajes, locacion, episodios, valorBuscador } = this.state;
        
        return (
            <Fragment>
                <Jumbotron fluid></Jumbotron>
                <Navbar bg="dark" variant="dark">
                    <FormControl type="text" placeholder="¿Qué buscas?" className="mr-sm-2 buscador" value={valorBuscador} onChange={this.buscarNombre} />
                </Navbar>

                <Container>
                    <div className="justify-content-center text-center pb-4">
                        <h2>Personajes</h2>
                    </div>
                    <Row className="justify-content-center">
                        {personajes
                            .filter(el => el.name.includes(valorBuscador))
                            .map((el, index) => {
                                return <Col className="col-tarjetas">
                                    <Card className="tarjetas">
                                        <FichaPersonaje key={index} image={el.image} name={el.name} />
                                        <Link className="btn btn-primary ver-mas" to={{
                                            pathname: "/personajes",
                                            state: { name: el.name, status: el.status, species: el.species, gender: el.gender, type: el.type, image: el.image },
                                            hash: `#${el.name}`
                                        }}>ver más</Link>
                                    </Card>
                                </Col>
                            })
                        }
                    </Row>
                    <div className="justify-content-center text-center py-4">
                        <h2>Locaciones</h2>
                    </div>
                    <Row className="justify-content-center">
                        {locacion
                            .filter(el => el.name.includes(valorBuscador))
                            .map((el, index) => {
                                 return <Col className="col-tarjetas">
                                    <Card className="tarjetas locacion">
                                        <FichaLocacion key={index} dimension={el.dimension} name={el.name} />
                                        <Link className="btn btn-primary ver-mas" to={{
                                            pathname: "/locacion",
                                            state: { name: el.name, type: el.type, dimension: el.dimension, residents: el.residents },
                                            hash: `#${el.name}`
                                        }}>ver más</Link>
                                    </Card>
                                </Col>
                            })
                        }
                    </Row>
                    <div className="justify-content-center text-center py-4">
                        <h2>Episodios</h2>
                    </div>
                    <Row className="justify-content-center">
                        {episodios
                            .filter(el => el.name.includes(valorBuscador))
                            .map((el, index) => {
                                return <Col className="col-tarjetas">
                                    <Card className="tarjetas locacion">
                                        <FichaEpisodios key={index} episode={el.episode} name={el.name} />
                                        <Link className="btn btn-primary ver-mas" to={{
                                            pathname: "/episodios",
                                            state: { name: el.name, air_date: el.air_date, episode: el.episode, characters: el.characters },
                                            hash: `#${el.name}`
                                        }}>ver más</Link>
                                    </Card>
                                </Col>
                            })
                        }
                    </Row>
                </Container>
                <Footer />
            </Fragment>
        );
    }
}

export default Inicio;