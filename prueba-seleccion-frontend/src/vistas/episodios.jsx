import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Episodios = (props) =>{
    return(
        <Container>
            <Row>
                <div className="ficha-personajes">
                   
                    <Col>
                        <ul className="listado-personajes">
                            <li>Nombre: {props.location.state.name}</li>
                            <li>Al aire: {props.location.state.air_date}</li>
                            <li>Episodio: {props.location.state.episode}</li>
                            <li>Personajes: {props.location.state.characters.length}</li>
                        </ul>
                        <Link className="volver-atras" to={{pathname: "/"}}>Volver atrás</Link>
                    </Col>
                </div>
            </Row>
        </Container>
    )
}

export default Episodios;