import React from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            usuarioAutenticado: false,
            respuestaData: ""
        };
        this.login = this.login.bind(this);
    }

    login = () => {

        const usuario = {
            "email": "eve.holt@reqres.in",
            "password": "cityslicka"
        }

        fetch('https://reqres.in/api/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json',
            },
            body: JSON.stringify(usuario)
        })
        .then(response => response.json())
        .then((data) => {
            this.setState({
                usuarioAutenticado: true,
                respuestaData: data.token
            });
        },
        (error) => {
            this.setState({
                usuarioAutenticado: false,
                respuestaData: "Datos no encontrados"
            });
        })
    }

    render() {

        const { usuarioAutenticado } = this.state;
        return (

            <Container>
                <Row>
                    <Col>

                    </Col>
                    <Col xs={12} sm={12} md={12} lg={6}>
                        <Form autoComplete="off" className="formularios">
                            <div className="mb-4">
                                <h1>Login</h1>
                            </div>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control
                                    type="email"
                                    placeholder="eve.holt@reqres.in"
                                    name="email"
                                    autoComplete="off"
                                />
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Contraseña</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="cityslicka"
                                    name="password"
                                    autoComplete="off"
                                />
                            </Form.Group>

                            <Button
                                variant="primary"
                                onClick={this.login}
                            >
                                Iniciar sesión
                            </Button>
                            {
                                usuarioAutenticado && <Redirect to="/" />
                            }
                        </Form>
                    </Col>
                    <Col></Col>
                </Row>
            </Container>
        );
    }
}

export default Login;