import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Locacion = (props) =>{
    return(
        <Container>
            <Row>
                <div className="ficha-personajes">
                   
                    <Col>
                        <ul className="listado-personajes">
                            <li>Nombre: {props.location.state.name}</li>
                            <li>Dimensión: {props.location.state.dimension}</li>
                            <li>Residentes: {props.location.state.residents.length}</li>
                            <li>Tipo: {props.location.state.type}</li>
                        </ul>
                        <Link className="volver-atras" to={{pathname: "/"}}>Volver atrás</Link>
                    </Col>
                </div>
            </Row>
        </Container>
    )
}

export default Locacion;