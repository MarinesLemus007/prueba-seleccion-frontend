import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Personajes = (props) => {
    return (
        <Container>
            <Row>
                <div className="ficha-personajes">
                    <Col>
                        <Image src={props.location.state.image} fluid />
                    </Col>
                    <Col>
                        <ul className="listado-personajes">
                            <li>Nombre: {props.location.state.name}</li>
                            <li>Estatus: {props.location.state.status}</li>
                            <li>Especie: {props.location.state.species}</li>
                            <li>Género: {props.location.state.gender}</li>
                            <li>Tipo: {props.location.state.type ? props.location.state.type : "Indefinido"}</li>
                        </ul>
                        <Link className="volver-atras" to={{pathname: "/"}}>Volver atrás</Link>
                    </Col>
                </div>
            </Row>
        </Container>
    )
}

export default Personajes;