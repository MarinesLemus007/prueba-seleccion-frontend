import React from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

class Registro extends React.Component {
    constructor() {
        super();
        this.state = {
            crearUsuario: false,
            respuestaData: ""
        };
        this.registro = this.registro.bind(this);
    }

    registro = () => {

        const usuario = {
            "email": "eve.holt@reqres.in",
            "password": "pistol"
        }

        fetch('https://reqres.in/api/register', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json',
            },
            body: JSON.stringify(usuario)
        })
        .then(response => response.json())
        .then((data) => {
            this.setState({
                crearUsuario: true,
                respuestaData: data.token
            });
        },
         (error) => {
            this.setState({
                crearUsuario: false,
                respuestaData: "Registro fallido"
            });
        })
    }


    render() {

        const { crearUsuario } = this.state;

        return (

            <Container>
                <Row>
                    <Col></Col>
                    <Col xs={12} sm={12} md={12} lg={6}>
                        <Form autoComplete="off" className="formularios">
                            <div className="mb-4">
                                <h1>Registro</h1>
                            </div>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control
                                    type="email"
                                    placeholder="eve.holt@reqres.in"
                                    name="email"
                                    autoComplete="off"
                                />
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Contraseña</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="pistol"
                                    name="password"
                                    autoComplete="off"
                                />
                            </Form.Group>

                            <Button
                                variant="primary"
                                onClick={this.registro}
                            >
                                Iniciar sesión
                            </Button>
                            {
                                crearUsuario && <Redirect to="/login" />
                            }
                        </Form>
                    </Col>
                    <Col></Col>
                </Row>
            </Container>
        );
    }
}

export default Registro;