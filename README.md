# Prueba de Experiencia (Frontend)
Esta prueba fue ideada para medir el nivel de conocimientos y experiencia tanto en sintaxis frontend (`javascript`) como el uso de API's.

## :computer: Tecnologías y dependencias usadas

* [ ] **React**
* [ ] **Javascript**
* [ ] **Firebase**
* [ ] **React Bootstrap**
* [ ] **React Router Dom**

## :memo: Planificación, prototipo y material de apoyo

Ver Trello (Tareas Cerradas) [ir a tablero](https://trello.com/b/QGQu2HQ6/reto-tactech-prueba-selecci%C3%B3n-frontend)

## :speak_no_evil: Sobre el desafío

Para simular el registro y login se trabajó con [Reqres](https://reqres.in). La página de inicio consume la Api [Rick and Morty API](https://rickandmortyapi.com/api/). El buscador filtra por nombre (es sensible a mayúsculas). El enrutado y paso de propiedades a las vistas de personajes, locacion y episodios se trabajaron con React Router Dom. Por otra parte, el consumo de las Apis se trabajaron con Fetch.

## :star2: Deploy

Deploy con Firebase [Ir a deploy](https://prueba-seleccion-frontend.web.app/registro)